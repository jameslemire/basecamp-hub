CREATE TABLE trackable_type (
  id SERIAL PRIMARY KEY,
  name VARCHAR,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP
);

INSERT INTO trackable_type (name)
VALUES ('flag'), ('bc_interaction');

CREATE TABLE trackable (
  id SERIAL PRIMARY KEY,
  enrollment_id INTEGER,
  type_id INTEGER,
  updated_by_user_id INTEGER,
  meta JSONB,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP
);

INSERT INTO trackable (enrollment_id, type_id, updated_by_user_id, meta, created_at)
VALUES (3167, 1, 1350, '{"resolved": false, "flagType": "underperforming", "notes": "this student needs help"}', NOW()),
(3167, 1, 1350, '{"resolved": true, "flagType": "no_show", "notes": "did not show up for meeting"}', NOW()),
(3167, 2, 884, '{"completion": 36, "confidence": 94, "notes": "these are some important notes"}', NOW());