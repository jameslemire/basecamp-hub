CREATE TABLE bc_mentor (
  id SERIAL PRIMARY KEY,
  user_id INTEGER,
  location_id INTEGER,
  capacity INTEGER,
  date_start DATE,
  date_end DATE,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE mentor_enrollment (
  id SERIAL PRIMARY KEY,
  mentor_id INTEGER,
  enrollment_id INTEGER,
  is_active BOOLEAN,
  session_id INTEGER,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  deleted_at TIMESTAMP
);

INSERT INTO bc_mentor (user_id, capacity, date_start, location_id )
VALUES (1350, 20, NOW(), 1), (3270, 18, NOW(), 1), (884, 10, NOW(), 2);

INSERT INTO mentor_enrollment (mentor_id, enrollment_id, is_active, session_id)
VALUES (1, 3167, true, 168), (1, 3170, true, 167), (1, 3243, false, 167);