CREATE TABLE email_template (
  id SERIAL PRIMARY KEY,
  "name" VARCHAR,
  "subject" VARCHAR,
  "from" VARCHAR,
  "to" VARCHAR,
  "text" TEXT,
  "variables" TEXT,
  "created_at" TIMESTAMP,
  "updated_at" TIMESTAMP
);

INSERT INTO email_template ("name", "subject", "from", "text", "variables") 
VALUES ('warning', 'DevMountain - Basecamp WARNING', 'basecamp@devmountain.com', E'Dear #[studentName], \n\n You are currently behind pace to start with your cohort. Completion of Basecamp, along with regular meetings with a Basecamp mentor, are required to begin at your start date. If progress is not made, your in class spot will be in jeopardy.', 'studentName');