const email = require('emailjs');
const server = email.server.connect({
  user: process.env.SMTP_USER,
  password: process.env.SMTP_PASSWORD,
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT
});

module.exports = {
  send: message => {
    return new Promise((resolve, reject) => {
      if (process.env.NODE_ENV === 'production') {
        server.send(message, (err, message) => {
          if (err) return reject(err);
          resolve(message);
        })
      } else {
        console.log('mocking sending an email :)');
        resolve(message);
      }
    })
  }
}