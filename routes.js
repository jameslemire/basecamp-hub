const passport = require('passport')

const { 
  AuthenticationController, 
  MentorsController, 
  StudentsController, 
  TrackablesController, 
  LocationsController,
  UsersController,
  EnrollmentController,
  SessionController,
  UserRoleController,
  UserRoleUserController ,
  EmailController
} = require('./controllers');

const isAdmin = require('./middleware/isAdmin');
const isMentor = require('./middleware/isMentor');

module.exports = app => {
   app.get('/auth/devmtn', passport.authenticate('devmtn'))
   app.get('/auth/devmtn/callback', passport.authenticate('devmtn', { failureRedirect: '/#/' }), AuthenticationController.loginSuccessRouter)
   app.get('/auth/me', (req, res) => {
      if (req.isAuthenticated()) {
         return res.send(req.user)
      } else {
         return res.status(403).send('not authenticated')
      }
   })

   app.get('/api/logout', AuthenticationController.clearJwtAuthCookie, AuthenticationController.logout)

  app.post('/api/email_template', EmailController.create);
  app.post('/api/email_template/:templateId/send', EmailController.sendEmail);

  app.get('/api/locations/mentors', LocationsController.locationsWithMentorInfo);
  app.get('/api/locations/cohorts', LocationsController.locationsWithCohortInfo);

   app.get('/api/users/with_mentor_role', UsersController.withMentorRole)

   app.get('/api/mentors', MentorsController.index)
   app.get('/api/mentors/available', MentorsController.available)
   app.get('/api/mentors/:id', MentorsController.show)
   app.get('/api/mentors/:user_id/students', MentorsController.getStudentsForMentor)
   app.delete('/api/mentors/:id', isAdmin, MentorsController.delete)
   app.post('/api/mentors', isAdmin, MentorsController.post)

   app.get('/api/students', StudentsController.index)
   app.get('/api/students/deferred', StudentsController.deferred)
   app.get('/api/students/without_mentor', StudentsController.withoutMentor)
   app.get('/api/students/dropped', StudentsController.droppedStudents)
   app.get('/api/students/flagged', StudentsController.flaggedStudents)
   app.get('/api/students/action_required', StudentsController.actionRequired)
   app.get('/api/students/danger_zone', StudentsController.dangerZoneStudents)
   app.post('/api/students/assign_mentor', isAdmin, StudentsController.assignMentor)
   app.delete('/api/students/:id', isAdmin, StudentsController.delete)

   app.post('/api/trackables/interaction/:enrollment_id', isMentor, TrackablesController.interaction)
   app.get('/api/trackables/flagged', TrackablesController.flagged)
   app.patch('/api/trackables/flagged/resolve/:id', isAdmin, TrackablesController.resolveFlaggedItem)

   // endpoints for syncing data when mysql db is updated
   app.post('/api/users', isAdmin, UsersController.create)
   app.put('/api/users/:id', isAdmin, UsersController.update)

   app.post('/api/enrollments', isAdmin, EnrollmentController.create)
   app.put('/api/enrollments/:id', isAdmin, EnrollmentController.update)

   app.post('/api/sessions', isAdmin, SessionController.create)
   app.put('/api/sessions/:id', isAdmin, SessionController.update)

   app.post('/api/user_roles', isAdmin, UserRoleController.create)
   app.post('/api/user_roles/:id', isAdmin, UserRoleController.update)

   app.post('/api/user_roles_users', isAdmin, UserRoleUserController.create)
}
