const express = require('express')
const session = require('express-session')
const passport = require('passport')
const RedisStore = require('connect-redis')(session)
const bodyParser = require('body-parser')
const cors = require('cors')
const { sequelize } = require('./models')
const config = require('./config/config')
const corsOptions = require('./config/cors')
const path = require('path')

const app = express()
app.use(bodyParser.json())

if (process.env.NODE_ENV === 'development') {
   app.use(cors(corsOptions))
}

app.use(
   session({
      secret: process.env.SESSION_SECRET || 'secret',
      store: new RedisStore({ host: 'redis_server' }),
      resave: false,
      saveUninitialized: false
   })
)
app.use(passport.initialize())
app.use(passport.session())

require('./routes')(app)
require('./init/passport')()

app.use(express.static(`${__dirname}/public/dist/`))
app.get('*', (req, res, next) => {
   res.sendFile(path.resolve(`${__dirname}/public/dist/index.html`))
})

sequelize.authenticate().then(() => {
   app.listen(config.port, () => {
      console.log(`Server started on port ${config.port}`)
   })
})
