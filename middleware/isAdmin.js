module.exports = (req, res, next) => {
   console.log('@req.user', req.user)
   console.log('@req.user.roles', req.user.roles)
   if (req.user && isAdmin(req.user.roles)) return next()
   return res.status(403).send('not authorized')
}

function isAdmin(roles) {
   return roles.indexOf('admin') !== -1
}
