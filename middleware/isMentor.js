module.exports = (req, res, next) => {
  if (req.user && isAdmin(req.user.roles)) return next();
  return res.status(403).send('not authorized');
}

function isMentor(roles) {
  return (
    roles.includes('mentor') || 
    roles.includes('admin') || 
    roles.includes('instructor') || 
    roles.includes('lead_instructor')) ||
    roles.includes('lead_mentor');
}