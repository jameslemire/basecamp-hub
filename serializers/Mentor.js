module.exports = {
  serialize: mentors => {
    return mentors.map(mentor => {
      let active_students_count = mentor.Enrollments.reduce((count, enrollment) => {
        enrollment.MentorEnrollment.is_active && count++;
        return count;
      }, 0);

      let { id, user_id, capacity } = mentor;
      let { first_name, last_name, phone, email } = mentor.User;

      return { id, user_id, first_name, last_name, phone, email, capacity, active_students_count };
    })
  }
}