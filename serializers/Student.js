module.exports = {
  serialize: students => {
    return students.map(student => {
      const enrollment_id = student.id; 
      const { first_name, last_name, email, phone } = student.User;
      const { short_name: cohort_name, date_start: cohort_start_date, date_end: cohort_end_date } = student.Session;
      let last_interaction =  student.Trackables[student.Trackables.length - 1];

      return { enrollment_id, first_name, last_name, email, phone, cohort_name, 
               cohort_start_date, cohort_end_date, last_interaction }
    })
  }
}