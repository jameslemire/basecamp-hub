const { Mentor } = require('../models');
const MentorSerializer = require('./Mentor');

module.exports = {
  serializeWithCohorts: async locations => {
    let results =  await Promise.all(locations.map(async location => {
      let cohorts = await Promise.all(location.Sessions.map(async cohort => {
        let students = await Promise.all(cohort.Enrollments.map(async student => {
          student.mentor = {};
          let most_recent_interaction = await student.getMostRecentInteraction();
          student.Mentors.forEach(mentor => {
            if (mentor.MentorEnrollment.is_active) {
              let { id } = mentor;
              let { first_name, last_name, id: user_id } = mentor.User;
              student.mentor = { id, first_name, last_name, user_id }; 
            }
          });
  
          let { id, mentor } = student;
          let { confidence, completion } = most_recent_interaction.meta;
          let { first_name, last_name, email, phone } = student.User;
          return { id, first_name, last_name, email, phone, mentor, confidence, completion };
        }));
  
        let { id, short_name: name } = cohort;
        return { id, name, students };
      }));
      let { id, name } = location;
      return { id, name, cohorts };
    }));

    return results;
  },

  serializeWithMentors: locations => {
    return locations.map(location => {
      let mentors = MentorSerializer.serialize(location.Mentors);
      let { id, name } = location;
      return { id, name, mentors };
    });
  }
}