const passport = require('passport');
const Devmtn = require('devmtn-auth');

const devmtnAuthConfig = require('../config/config').auth;
const { User } = require('../models');

const DevmtnStrategy = Devmtn.Strategy;

passport.serializeUser((user, done) => {
  console.log('serializing user:', user);
  done(null, user.id);
});

passport.deserializeUser(async (userId, done) => {
  console.log("this is the user id stored on the session:", userId);
  try {
    const user = await User.findOne({ 
      where: { id: userId },
      include: ['UserRoles']
    });
    
    const { id, email, first_name, last_name, } = user;
    const roles = user.UserRoles.map(role => role.role);
    
    const serializedUser = { id, email, first_name, last_name, roles }
    done(null, serializedUser)
  } catch (error) {
    console.error('there was an error fetching user info', error)
    return done(error);
  }

});

module.exports = () => {
  passport.use('devmtn', new DevmtnStrategy(devmtnAuthConfig, function (jwtoken, user, done) {
    return done(null, user);
  }));
}