import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Landing/Landing'
import Cohorts from '@/components/Cohorts/Cohorts'
import Mentors from '@/components/Mentors/Mentors'
import Interactions from '@/components/Interactions/Interactions'

Vue.use(Router)

export default new Router({
   mode: 'history',
   routes: [
      {
         path: '/',
         component: Landing
      },
      {
         path: '/cohorts',
         component: Cohorts
      },
      {
         path: '/mentors',
         component: Mentors
      },
      {
         path: '/interactions',
         component: Interactions
      }
   ]
})
