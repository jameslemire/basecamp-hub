import axios from 'axios'
import config from '@/config'

export default () =>
   axios.get(`${config.api.baseUrl}${config.api.routes.auth.check}`).catch(err => {
      console.error('User is not authenticated:', err)
      window.location.assign(`${config.api.baseUrl}${config.api.routes.auth.notAuthorized}`)
   })
