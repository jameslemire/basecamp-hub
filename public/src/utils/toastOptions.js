export default {
   error: {
      theme: 'bubble',
      position: 'bottom-right',
      duration: 5000,
      className: 'toast-error'
   },

   success: {
      theme: 'bubble',
      position: 'bottom-right',
      duration: 5000,
      className: 'toast-success'
   }
}
