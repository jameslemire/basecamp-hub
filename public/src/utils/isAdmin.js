export default roles => {
   const allowedRoles = ['admin', 'lead_instructor', 'instructor']
   let isAdmin = false

   allowedRoles.forEach(role => {
      if (roles.indexOf(role) !== -1) {
         isAdmin = true
      }
   })

   return isAdmin
}
