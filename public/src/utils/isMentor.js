export default roles => {
   let isMentor = false

   if (roles.indexOf('mentor') !== -1) {
      isMentor = true
   }

   return isMentor
}
