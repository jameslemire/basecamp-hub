import Email from '@/components/Landing/Modal/Menus/Email'
import Assign from '@/components/Landing/Modal/Menus/Assign'
import Resolve from '@/components/Landing/Modal/Menus/Resolve'

export const assignMentor = {
   id: 'ai_0',
   menu: 'myAssign',
   text: 'Assign Mentor'
}

export const resolveAction = {
   id: 'ai_1',
   menu: 'myResolve',
   text: 'Resolve Action'
}

export const emailStudent = {
   id: 'dz_0',
   menu: 'myEmail',
   text: 'Email Student'
}

function generateMixin() {
   return {
      data() {
         return {
            actionItems: {
               general: [resolveAction],
               deferred: [assignMentor],
               unassigned: [assignMentor]
            },
            dangerZone: [emailStudent]
         }
      },
      components: {
         myEmail: Email,
         myAssign: Assign,
         myResolve: Resolve
      }
   }
}

export default generateMixin()
