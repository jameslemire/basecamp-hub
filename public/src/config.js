export default generateApiConfig()

export function generateApiConfig() {
   let baseUrl = ''

   if (process.env.NODE_ENV === 'development') {
      baseUrl = 'http://localhost:4000'
   } else if (process.env.NODE_ENV === 'test') {
      baseUrl = 'http://localhost:4000'
   } else if (process.env.NODE_ENV === 'production') {
      baseUrl = 'http://104.236.251.233'
   } else {
      console.error('Unknown NODE_ENV')
      baseUrl = 'unknown'
   }

   return {
      api: {
         baseUrl,
         routes: {
            mentors: {
               available_assign: '/api/mentors/available',
               available_add: '/api/users/with_mentor_role',
               add_mentor: '/api/mentors',
               assign_mentor: '/api/students/assign_mentor'
            },
            students: {
               action: '/api/students/action_required',
               danger: '/api/students/danger_zone',
               assigned$: '/api/mentors/$user_id/students'
            },
            locations: {
               mentors: '/api/locations/mentors',
               cohorts: '/api/locations/cohorts'
            },
            auth: {
               check: '/auth/me',
               notAuthorized: '/auth/devmtn'
            },
            email: {
               warning: '/api/email_template/warning/send'
            },
            interactions: {
               create$: '/api/trackables/interaction/$enrollment_id'
            },
            resolve$: '/api/trackables/flagged/resolve/$trackable_id'
         }
      }
   }
}
