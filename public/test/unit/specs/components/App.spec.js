import App from '@/App.vue'
import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Header from '@/components/Header/Header'

const localVue = createLocalVue()
localVue.use(VueRouter)

const routes = [
   { path: '/', components: Header }
]

const router = new VueRouter({
   mode: 'history',
   routes
})

describe('App.vue', () => {
   it('should render successfully with a Header component', () => {
      let instance = mount(App, {
         localVue,
         router
      })

      expect(instance.contains('header')).toEqual(true)

      instance.destroy()
   })
})
