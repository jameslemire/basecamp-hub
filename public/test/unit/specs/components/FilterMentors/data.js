export default [
   {
      first_name: 'first0',
      last_name: 'last0',
      user_id: 0
   },
   {
      first_name: 'first1',
      last_name: 'last1',
      user_id: 1
   },
   {
      first_name: 'first2',
      last_name: 'last2',
      user_id: 2
   }
]
