import FilterMentors from '@/components/FilterMentors/FilterMentors'
import { mount } from '@vue/test-utils'
import data from './data'
import moment from 'moment'

let instance
const now = new Date()

const fakeData = {
   userFilter: 'filter',
   selectedMentor: { id: 0, first_name: 'first0', last_name: 'last0' },
   dateStart: now,
   capacity: 1
}

describe('FilterMentor.vue', () => {
   beforeEach(() => {
      instance = mount(FilterMentors, {
         propsData: {
            mentors: data,
            isAdd: false
         },
         data: fakeData
      })
   })

   afterEach(() => {
      instance.destroy()
   })

   describe('formatStartDate computed property', () => {
      it('should exist', () => {
         const propertyExists = typeof instance.vm.$options.computed.formatStartDate === 'function'
         expect(propertyExists).toEqual(true)
      })

      it('should return a valid date', () => {
         const date = instance.vm.$options.computed.formatStartDate()
         const dateIsValid = moment(date).isValid()
         expect(dateIsValid).toEqual(true)
      })
   })

   describe('capacityInt computed property', () => {
      it('should exist', () => {
         const propertyExists = typeof instance.vm.$options.computed.capacityInt === 'function'
         expect(propertyExists).toEqual(true)
      })

      it('should the capacity as an int', () => {
         instance.vm.capacity = '45'
         instance.vm.$options.computed.capacityInt()

         const int = instance.vm.capacityInt

         expect(typeof int).toEqual('number')
         expect(int).toEqual(45)
      })
   })

   describe('filteredMentors computed property', () => {
      it('should exist', () => {
         const propertyExists = typeof instance.vm.$options.computed.filteredMentors === 'function'
         expect(propertyExists).toEqual(true)
      })

      it('should return null if there is no data.userFilter', () => {
         const result = instance.vm.$options.computed.filteredMentors()
         expect(result).toEqual(null)
      })

      it('should filter mentors by first and last name', () => {
         let filteredMentors

         instance.vm.userFilter = '0'
         instance.vm.$options.computed.filteredMentors()

         filteredMentors = instance.vm.filteredMentors

         expect(filteredMentors.length).toEqual(1)

         instance.vm.userFilter = 'first'
         instance.vm.$options.computed.filteredMentors()

         filteredMentors = instance.vm.filteredMentors

         expect(filteredMentors.length).toEqual(3)

         instance.vm.userFilter = 'last'
         instance.vm.$options.computed.filteredMentors()

         filteredMentors = instance.vm.filteredMentors

         expect(filteredMentors.length).toEqual(3)

         instance.vm.userFilter = '2'
         instance.vm.$options.computed.filteredMentors()

         filteredMentors = instance.vm.filteredMentors

         expect(filteredMentors.length).toEqual(1)
      })
   })

   describe('methods.emitAllData', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.emitAllData === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should use $emit with "allData" as the first argument and an object as the second argument', () => {
         instance.vm.$emit = jest.fn()
         instance.vm.emitAllData()

         const mock = instance.vm.$emit.mock

         expect(mock.calls.length).toEqual(1)
         expect(mock.calls[0][0]).toEqual('allData')
         expect(typeof mock.calls[0][1]).toEqual('object')
         expect(mock.calls[0][1] !== null).toEqual(true)
      })
   })

   describe('Watch Methods', () => {
      let mock

      beforeEach(() => {
         instance.vm.emitAllData = jest.fn()
         mock = instance.vm.emitAllData.mock
      })

      it('should watch for changes to data.selectedMentor and call methods.emitAllData', () => {
         instance.setData({ selectedMentor: {} })
         expect(mock.calls.length).toEqual(1)
      })

      it('should watch for changes to data.dateStart and call methods.emitAllData', () => {
         instance.setData({ dateStart: new Date() })
         expect(mock.calls.length).toEqual(1)
      })

      it('should watch for changes to data.capacity and call methods.emitAllData', () => {
         instance.setData({ capacity: 3 })
         expect(mock.calls.length).toEqual(1)
      })
   })
})
