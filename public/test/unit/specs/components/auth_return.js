export default {
   admin: {
      id: 0,
      email: 'email',
      first_name: 'first',
      last_name: 'last',
      roles: ['admin']
   },
   mentor: {
      id: 0,
      email: 'email',
      first_name: 'first',
      last_name: 'last',
      roles: ['mentor']
   }
}
