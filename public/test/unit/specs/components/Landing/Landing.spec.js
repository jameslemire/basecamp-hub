import Landing from '@/components/Landing/Landing'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import config from '@/config.js'
import { mount, createLocalVue } from '@vue/test-utils'
import actionData from './actionData'
import dangerData from './dangerData'
import icepick from 'icepick'
import authReturn from '../auth_return'
import Toasted from 'vue-toasted'

const selected_id = actionData[1].id
const selected_index = 1

let instance, consoleSpy, resetSpy, toastSpy, removeSpy, closeSpy
const mock = new MockAdapter(axios)
let fakeData = {
   students_action: actionData,
   selected: {
      student: { enrollment_id: 0, session_id: 1, id: selected_id },
      type: 'Action Item',
      index: selected_index
   }
}

// Allow Toasted to work in Unit Tests
const localVue = createLocalVue()
localVue.use(Toasted)

icepick.freeze(actionData)
icepick.freeze(dangerData)

// API
const authURL = `${config.api.baseUrl}${config.api.routes.auth.check}`
const actionURL = `${config.api.baseUrl}${config.api.routes.students.action}`
const dangerURL = `${config.api.baseUrl}${config.api.routes.students.danger}`
const assignURL = `${config.api.baseUrl}${config.api.routes.mentors.assign_mentor}`

const fakeTrackableId = 0
const resolveURL = `${config.api.baseUrl}${config.api.routes.resolve$.replace('$trackable_id', fakeTrackableId)}`

describe('Landing.vue', () => {
   beforeEach(() => {
      mock.onGet(authURL).reply(200, authReturn.admin)
      mock.onGet(actionURL).reply(200, actionData)
      mock.onGet(dangerURL).reply(200, dangerData)
      mock.onPost(assignURL).reply(200, {})
      mock.onPatch(resolveURL).reply(200, {})
      instance = mount(Landing, { localVue, data: fakeData })
      consoleSpy = jest.spyOn(global.console, 'error')
      toastSpy = jest.spyOn(instance.vm.$toasted, 'show')
      removeSpy = jest.spyOn(instance.vm, 'removeStudentFromTable')
   })

   afterEach(() => {
      instance.destroy()
      consoleSpy.mockReset()
      toastSpy.mockReset()
      removeSpy.mockReset()
   })

   it('should render the action-items component', () => {
      // Call update so the v-if renders action-items
      instance.setData({ fetchedData: true })
      const componentExists = instance.find({ ref: 'action-items-component' }).exists()
      expect(componentExists).toEqual(true)
   })

   it('should render the danger-zone component', () => {
      // Call update so the v-if renders action-items
      instance.setData({ fetchedData: true })
      const componentExists = instance.find({ ref: 'danger-zone-component' }).exists()
      expect(componentExists).toEqual(true)
   })

   describe('fetchStudents method', () => {
      beforeEach(() => {
         consoleSpy.mockReset()
      })

      it('should set data.students_action and data.students_danger with results from API', done => {
         expect.assertions(2)

         instance.vm.fetchStudents().then(() => {
            expect(instance.vm.students_action).toEqual(actionData)
            expect(instance.vm.students_danger).toEqual(dangerData)
            done()
         })
      })

      it('should set data.fetchedData to true after successful API request', done => {
         expect.assertions(1)

         instance.vm.fetchStudents().then(() => {
            expect(instance.vm.fetchedData).toEqual(true)
            done()
         })
      })

      it('should call console.error and $toasted.show on a failed API request', done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.students.danger}`).reply(500, 'error')

         expect.assertions(2)

         instance.vm.fetchStudents().then(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            expect(toastSpy.mock.calls.length).toEqual(1)
            done()
         })
      })

      it('should return null if user is not an admin', async done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(200, authReturn.mentor)

         expect.assertions(1)

         const result = await instance.vm.fetchStudents()
         expect(result).toEqual(null)
         done()
      })
   })

   describe('showModal method', () => {
      let methods
      const student = { id: 0, name: 'Bob' }
      const index = 0
      const type = 'Danger Zone'
      const defaultedMenu = 'myEmail'

      beforeEach(() => {
         methods = instance.vm.$options.methods
         instance.vm.showModal(student, type, index, defaultedMenu)
      })

      it('should exist', () => {
         const methodExists = typeof methods.showModal === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should set selected to the passed in student and type', () => {
         expect(instance.vm.selected).toEqual({ student, type, index })
      })

      it('should set defaultedMenu to the passed in defaulted menu', () => {
         expect(instance.vm.defaultedMenu).toEqual(defaultedMenu)
      })

      it('should set modalVisible to true', () => {
         expect(instance.vm.modalVisible).toEqual(true)
      })
   })

   describe('closeModal method', () => {
      let methods

      beforeEach(() => {
         methods = instance.vm.$options.methods
         instance.vm.showModal({}, 'Danger Zone', 'myEmail')
         instance.vm.closeModal()
      })

      it('should exist', () => {
         const methodExists = typeof methods.closeModal === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should set selected back to the default', () => {
         const student = instance.vm.selected.student
         const type = instance.vm.selected.type

         expect(student).toEqual({})
         expect(type).toEqual('')
      })

      it('should set modalVisible to false', () => {
         expect(instance.vm.modalVisible).toEqual(false)
      })
   })

   describe('resetStudentsAction method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.resetStudentsAction === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should reset data.modalVisible and data.selected back to their defaults', () => {
         instance.setData({
            modalVisible: true,
            selected: {
               student: { id: 0, name: 'bob' },
               type: 'Action Item',
               index: 2
            }
         })

         instance.vm.resetStudentsAction()

         expect(instance.vm.modalVisible).toEqual(false)
         expect(instance.vm.selected).toEqual({
            student: {},
            type: '',
            index: null
         })
      })
   })

   describe('assignMentor method', () => {
      beforeEach(() => {
         resetSpy = jest.spyOn(instance.vm, 'resetStudentsAction')
      })

      afterEach(() => {
         resetSpy.mockReset()
      })

      it('should call vm.resetStudentsAction and vm.removeStudentFromTable', done => {
         const mentorId = 1

         expect.assertions(3)

         instance.vm.assignMentor(mentorId).then(() => {
            expect(resetSpy.mock.calls.length).toEqual(1)
            expect(instance.vm.students_action.length).toEqual(actionData.length - 1)
            expect(removeSpy.mock.calls.length).toEqual(1)
            done()
         })
      })

      it('should call console.error and $toast.show on failed API request', done => {
         const mentorId = 1
         mock.onPost(`${config.api.baseUrl}${config.api.routes.mentors.assign_mentor}`).reply(500, 'err')

         expect.assertions(2)

         instance.vm.assignMentor(mentorId).then(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            expect(toastSpy.mock.calls.length).toEqual(1)
            done()
         })
      })
   })

   describe('removeStudentFromTable method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.removeStudentFromTable === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should splice out a student by index', () => {
         instance.vm.removeStudentFromTable(0)
         // findIndex returns -1 if an element is not in an array
         let index = instance.vm.students_action.findIndex(student => student.id == actionData[0].id)
         expect(index).toEqual(-1)
      })

      it('should splice out a student by index', () => {
         instance.vm.removeStudentFromTable(1)
         // findIndex returns -1 if an element is not in an array
         let index = instance.vm.students_action.findIndex(student => student.id == actionData[1].id)
         expect(index).toEqual(-1)
      })

      it('should splice out a student by index', () => {
         instance.vm.removeStudentFromTable(2)
         // findIndex returns -1 if an element is not in an array
         let index = instance.vm.students_action.findIndex(student => student.id == actionData[2].id)
         expect(index).toEqual(-1)
      })
   })

   describe('resolveAction method', () => {
      beforeEach(() => {
         removeSpy = jest.spyOn(instance.vm, 'removeStudentFromTable')
         closeSpy = jest.spyOn(instance.vm, 'closeModal')
      })

      afterEach(() => {
         removeSpy.mockReset()
         closeSpy.mockReset()
      })

      it('should exist', () => {
         const methodExists = typeof instance.vm.resolveAction === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should splice the student from the table and close the modal on success', done => {
         expect.assertions(2)

         instance.vm.resolveAction(fakeTrackableId).then(() => {
            expect(removeSpy.mock.calls.length).toEqual(1)
            expect(closeSpy.mock.calls.length).toEqual(1)
            done()
         })
      })

      it('should use console.error and $toasted.show on failure', done => {
         mock.onPatch(resolveURL).reply(500, 'error')

         expect.assertions(4)

         instance.vm.resolveAction(fakeTrackableId).then(() => {
            expect(removeSpy.mock.calls.length).toEqual(0)
            expect(closeSpy.mock.calls.length).toEqual(0)
            expect(consoleSpy.mock.calls.length).toEqual(1)
            expect(toastSpy.mock.calls.length).toEqual(1)
            done()
         })
      })
   })
})
