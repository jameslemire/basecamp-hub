import moment from 'moment'

const now = moment().toISOString()

export default [
   {
      id: 0,
      first_name: 'first0',
      last_name: 'last0',
      date_enrolled: now,
      cohort_start_date: now,
      cohort_name: 'cohort0',
      last_interaction: {
         meta: {
            completion: 30
         }
      }
   },
   {
      id: 1,
      first_name: 'first1',
      last_name: 'last1',
      date_enrolled: now,
      cohort_start_date: now,
      cohort_name: 'cohort1',
      last_interaction: {
         meta: {
            completion: 30
         }
      }
   },
   {
      id: 2,
      first_name: 'first2',
      last_name: 'last2',
      date_enrolled: now,
      cohort_start_date: now,
      cohort_name: 'cohort2',
      last_interaction: {
         meta: {
            completion: 30
         }
      }
   }
]
