import moment from 'moment'

const now = moment().toISOString()

export default [
   {
      enrollment_id: 0,
      cohort_start_date: now,
      first_name: 'first0',
      last_name: 'last0'
   },
   {
      enrollment_id: 1,
      cohort_start_date: now,
      first_name: 'first1',
      last_name: 'last1'
   }
]
