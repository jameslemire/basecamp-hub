import Interactions from '@/components/Interactions/Interactions'
import { mount } from '@vue/test-utils'
import axios from 'axios'
import config from '@/config'
import MockAdapter from 'axios-mock-adapter'
import authReturn from '../auth_return'
import data from './data'

let instance
let consoleSpy
const mock = new MockAdapter(axios)

// API endpoints
const authCheckUrl = `${config.api.baseUrl}${config.api.routes.auth.check}`
const assignedStudentsUrl = `${config.api.baseUrl}${config.api.routes.students.assigned$.replace('$user_id', authReturn.mentor.id)}`

describe('Interactions.vue', () => {
   beforeEach(() => {
      mock.onGet(authCheckUrl).reply(200, authReturn.mentor)
      mock.onGet(assignedStudentsUrl).reply(200, data)
      consoleSpy = jest.spyOn(global.console, 'error')
      instance = mount(Interactions)
   })

   afterEach(() => {
      instance.destroy()
      consoleSpy.mockReset()
   })

   describe('fetchAssignedStudents method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.fetchAssignedStudents === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should fetch assigned students by user_id and assig them to data.students', done => {
         expect.assertions(1)

         instance.vm.fetchAssignedStudents().then(() => {
            expect(instance.vm.students).toEqual(data)
            done()
         })
      })

      it('should fetch assigned students by user_id and set data.fetchedData to true', done => {
         expect.assertions(1)

         instance.vm.fetchAssignedStudents().then(() => {
            expect(instance.vm.fetchedData).toEqual(true)
            done()
         })
      })

      it('should call console.error on a failed API request', done => {
         mock.onGet(assignedStudentsUrl).reply(500, 'error')
         expect.assertions(1)

         // Use a setTimeout because fetchAssignedStudents is called in the created lifecycle method
         setTimeout(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            done()
         }, 1)
      })

      it('should return null if user is not an admin', async done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(200, authReturn.admin)

         expect.assertions(1)

         const result = await instance.vm.fetchAssignedStudents()

         expect(result).toEqual(null)
         done()
      })
   })

   describe('createInteraction method', () => {
      const enrollment_id = 0
      const meta = { confidence: 30, completion: 45, notes: 'i am note' }
      let methodSpy

      beforeEach(() => {
         methodSpy = jest.spyOn(instance.vm, 'cancelInteraction')
      })

      afterEach(() => {
         methodSpy.mockReset()
      })

      it('should exist', () => {
         const methodExists = typeof instance.vm.createInteraction === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should send a post request to the API to add a new interaction', done => {
         mock.onPost(`${config.api.baseUrl}${config.api.routes.interactions.create$.replace('$enrollment_id', enrollment_id)}`).reply(200, {})

         instance.vm.createInteraction(enrollment_id, meta).then(() => {
            expect(consoleSpy.mock.calls.length).toEqual(0)
            done()
         })
      })

      it('should call the cancelInteraction method on success', done => {
         mock.onPost(`${config.api.baseUrl}${config.api.routes.interactions.create$.replace('$enrollment_id', enrollment_id)}`).reply(200, {})

         instance.vm.createInteraction(enrollment_id, meta).then(() => {
            expect(methodSpy.mock.calls.length).toEqual(1)
            done()
         })
      })

      it('should call console.error on a failed API request', done => {
         mock.onPost(`${config.api.baseUrl}${config.api.routes.interactions.create$.replace('$enrollment_id', enrollment_id)}`).reply(500, 'error')

         instance.vm.createInteraction(enrollment_id, meta).then(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            done()
         })
      })
   })

   describe('cancelInteraction method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.cancelInteraction === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should set data.selectedStudent to null', () => {
         instance.setData({ selectStudent: {} })
         instance.vm.cancelInteraction()
         expect(instance.vm.selectedStudent).toEqual(null)
      })
   })
})
