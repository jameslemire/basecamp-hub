export default [
   {
      id: 0,
      cohorts: [
         {
            id: 0,
            name: 'COH0'
         },
         {
            id: 1,
            name: 'COH1'
         },
         {
            id: 2,
            name: 'COH2'
         }
      ],
      name: 'campuse0'
   },
   {
      id: 1,
      cohorts: [
         {
            id: 1,
            name: 'COH1'
         }
      ],
      name: 'campuse1'
   },
   {
      id: 2,
      cohorts: [
         {
            id: 2,
            name: 'COH2'
         }
      ],
      name: 'campuse2'
   }
]
