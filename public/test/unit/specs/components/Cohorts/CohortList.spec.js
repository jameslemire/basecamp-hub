import CohortList from '@/components/Cohorts/CohortList/CohortList.vue'
import { mount } from '@vue/test-utils'
import icepick from 'icepick'
import data from './data'

icepick.freeze(data)

let instance
let instanceMethods
const fakeProps = { cohorts: data[0].cohorts }

describe('CohortList.vue', () => {
   beforeEach(() => {
      instance = mount(CohortList, {
         propsData: fakeProps
      })
      instanceMethods = instance.vm.$options.methods
   })

   afterEach(() => {
      instance.destroy()
   })

   describe('mounting', () => {
      it('should render 3 buttons if given 3 cohorts', () => {
         expect(instance.findAll('button').length).toEqual(3)
      })
   })

   describe('selectCohort method', () => {
      it('should exist', () => {
         expect(instanceMethods.selectCohort).toBeDefined()
      })

      it('should set selected on data to the passed in campus', () => {
         // Find the first button on the DOM and click it
         instance.find('button').trigger('click')
         expect(instance.vm.selected).toEqual(fakeProps.cohorts[0])
      })

      it('should call $emit with "selected" and the selected cohort as arguments', () => {
         // Mock the $emit function to track calls/arguments
         instance.vm.$emit = jest.fn()
         // Find all buttons on the DOM and click the second one
         instance.findAll('button').at(1).trigger('click')

         const mock = instance.vm.$emit.mock
         expect(mock.calls.length).toEqual(1)
         expect(mock.calls[0][0]).toEqual('selected')
         expect(mock.calls[0][1]).toEqual(fakeProps.cohorts[1])
      })
   })
})
