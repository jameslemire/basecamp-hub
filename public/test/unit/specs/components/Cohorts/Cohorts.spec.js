import Cohorts from '@/components/Cohorts/Cohorts'
import { mount, createLocalVue } from '@vue/test-utils'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import config from '@/config'
import icepick from 'icepick'
import data from './data'
import authReturn from '../auth_return'
import Toasted from 'vue-toasted'

icepick.freeze(data)

let instance
let consoleSpy
const mock = new MockAdapter(axios)
const fakeData = {
   selectedCohort: null,
   selectedCampus: null,
   campuses: null,
   fetchedData: false
}

const localVue = createLocalVue()
localVue.use(Toasted)

describe('Cohorts.vue', () => {
   beforeEach(() => {
      mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(200, authReturn.admin)
      mock.onGet(`${config.api.baseUrl}${config.api.routes.locations.cohorts}`).reply(200, data)
      instance = mount(Cohorts, { localVue, data: fakeData })
      consoleSpy = jest.spyOn(global.console, 'error')
   })

   afterEach(() => {
      instance.destroy()
      consoleSpy.mockReset()
   })

   it('should render successfully with the CampusList shown and CohortList/StudentTable hidden', () => {
      instance.setData({ fetchedData: true })

      expect(instance.find({ ref: 'campus-list-component' }).exists()).toEqual(true)
      expect(instance.find({ ref: 'cohort-list-component' }).exists()).toEqual(false)
      expect(instance.find({ ref: 'student-table-component' }).exists()).toEqual(false)
   })

   it('should display a loading svg before data has come in', () => {
      instance.setData({ fetchedData: false })
      const loaderIsRendered = instance.find({ ref: 'load-spinner' }).exists()
      expect(loaderIsRendered).toEqual(true)
   })

   it('should display the CampusList/CohortList/StudentTable components when data has come in', () => {
      instance.setData({ fetchedData: true, campuses: [], selectedCampus: { cohorts: [] }, selectedCohort: { name: 'cohort', students: [] } })

      expect(instance.find({ ref: 'campus-list-component' }).exists()).toEqual(true)
      expect(instance.find({ ref: 'cohort-list-component' }).exists()).toEqual(true)
      expect(instance.find({ ref: 'student-table-component' }).exists()).toEqual(true)
   })

   describe('fetchCohorts method', () => {
      it('should update data.campuses with returned data from API', done => {
         expect.assertions(1)

         instance.vm.fetchCohorts().then(() => {
            expect(instance.vm.campuses).toEqual(data)
            done()
         })
      })

      it('should use console.error and $toasted.show on failed API request', done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.locations.cohorts}`).reply(500, 'err')

         expect.assertions(1)

         // Use a setTimeout because the fetchCohorts method gets called automatically in the created life cycle method
         setTimeout(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            done()
         }, 1)
      })

      it('should return null if user is not an admin', async done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(200, authReturn.mentor)

         expect.assertions(1)

         const result = await instance.vm.fetchCohorts()

         expect(result).toEqual(null)
         done()
      })
   })
})
