import CampusList from '@/components/CampusList/CampusList'
import { mount } from '@vue/test-utils'

let instance
let instanceMethods
const fakeProps = { campuses: [ { id: 0, name: 'name0' }, { id: 1, name: 'name1' }, { id: 2, name: 'name2' } ] }

describe('CampusList.vue', () => {
   beforeEach(() => {
      instance = mount(CampusList, {
         propsData: fakeProps
      })
      instanceMethods = instance.vm.$options.methods
   })

   afterEach(() => {
      instance.destroy()
   })

   describe('mounting', () => {
      it('should render 3 buttons if given 3 campuses', () => {
         expect(instance.findAll('button').length).toEqual(3)
      })
   })

   describe('selectCampus method', () => {
      it('should exist', () => {
         expect(instanceMethods.selectCampus).toBeDefined()
      })

      it('should set selected on data to the passed in campus', () => {
         // Find the first button on the DOM and click it
         instance.find('button').trigger('click')
         expect(instance.vm.selected).toEqual(fakeProps.campuses[0])
      })

      it('should call $emit with "selected" and the selected campus as arguments', () => {
         // Mock the $emit function to track calls/arguments
         instance.vm.$emit = jest.fn()
         // Find all buttons on the DOM and click the second one
         instance.findAll('button').at(1).trigger('click')

         const mock = instance.vm.$emit.mock
         expect(mock.calls.length).toEqual(1)
         expect(mock.calls[0][0]).toEqual('selected')
         expect(mock.calls[0][1]).toEqual(fakeProps.campuses[1])
      })
   })
})
