import AddMentor from '@/components/Mentors/Modal/AddMentor'
import { mount } from '@vue/test-utils'
import data from './data'

let instance

describe('AddMentor.vue', () => {
   beforeEach(() => {
      instance = mount(AddMentor, {
         propsData: {
            closeModal: jest.fn(),
            mentors: data,
            locationId: 1,
            addMentorToLocation: jest.fn()
         }
      })
   })

   afterEach(() => {
      instance.destroy()
   })

   it('should initialize with data.filterData as an empty object', () => {
      const dataExists = typeof instance.vm.filterData === 'object' && instance.vm.filterData !== null
      expect(dataExists).toEqual(true)
   })
})
