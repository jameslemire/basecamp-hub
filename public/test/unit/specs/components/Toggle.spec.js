import Toggle from '@/components/Toggle/Toggle.vue'
import { mount } from '@vue/test-utils'

let instance

describe('Toggle.vue', () => {
   beforeEach(() => {
      instance = mount(Toggle, {
         propsData: {
            visible: true
         }
      })
   })

   afterEach(() => {
      instance.destroy()
   })

   it('should emit a toggle event when clicking on it', () => {
      instance.vm.$emit = jest.fn()
      instance.find({ ref: 'toggle-container' }).trigger('click');

      const mock = instance.vm.$emit.mock
      expect(mock.calls.length).toEqual(1)
      expect(mock.calls[0][0]).toEqual('toggle')
   })

   it('should have a toggle method', () => {
      expect(instance.vm.$options.methods.toggle).toBeDefined()
   })
})
