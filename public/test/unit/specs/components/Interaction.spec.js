import Interaction from '@/components/Interactions/Interaction/Interaction'
import { mount } from '@vue/test-utils'

let instance
let fakeProps = {
   student: {
      enrollment_id: 0,
      first_name: 'first0',
      last_name: 'last0'
   },
   createInteraction: jest.fn(),
   cancelInteraction: jest.fn()
}

describe('Interaction.vue', () => {
   beforeEach(() => {
      instance = mount(Interaction, {
         propsData: fakeProps
      })
   })

   afterEach(() => {
      instance.destroy()
      fakeProps.createInteraction.mockClear()
      fakeProps.cancelInteraction.mockClear()
   })

   describe('confidence computed method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.$options.computed.confidence === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should return data.confidenceString as an INT', () => {
         instance.setData({ confidenceString: '30' })
         const confidenceInt = instance.vm.confidence
         expect(typeof confidenceInt).toEqual('number')
         expect(confidenceInt).toEqual(30)
      })
   })

   describe('completion computed method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.$options.computed.completion === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should return data.completionString as an INT', () => {
         instance.setData({ completionString: '76' })
         const completionInt = instance.vm.completion
         expect(typeof completionInt).toEqual('number')
         expect(completionInt).toEqual(76)
      })
   })
})
