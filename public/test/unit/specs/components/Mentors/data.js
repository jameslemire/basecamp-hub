export default {
   get: [
      {
         id: 0,
         active_students_count: 1,
         capacity: 2,
         email: 'email0',
         first_name: 'first0',
         last_name: 'last0',
         phone: '0',
         used_id: 0
      },
      {
         id: 1,
         active_students_count: 3,
         capacity: 4,
         email: 'email1',
         first_name: 'first1',
         last_name: 'last1',
         phone: '1',
         used_id: 1
      },
      {
         id: 2,
         active_students_count: 4,
         capacity: 5,
         email: 'email2',
         first_name: 'first2',
         last_name: 'last2',
         phone: '2',
         used_id: 2
      }
   ],
   post: {
         id: 0,
         user_id: 1,
         location_id: 2,
         capacity: 3,
         date_start: '2018-03-14',
         updated_at: '2018-03-12T20:20:16.413Z',
         created_at: '2018-03-12T20:20:16.413Z',
         date_end: null,
         deleted_at: null
   }
}
