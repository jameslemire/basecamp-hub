import Mentors from '@/components/Mentors/Mentors'
import { mount, createLocalVue } from '@vue/test-utils'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import config from '@/config.js'
import data from './data'
import icepick from 'icepick'
import moment from 'moment'
import authReturn from '../auth_return'
import Toasted from 'vue-toasted'

let instance, consoleSpy, toastSpy
const mock = new MockAdapter(axios)
const fakeArguments = {
   mentor: { user_id: 0, first_name: 'james', last_name: 'lemire' },
   capacity: 5,
   dateStart: moment().toISOString(),
   locationId: 1
}

const localVue = createLocalVue()
localVue.use(Toasted)

icepick.freeze(data)

describe('Mentors.vue', () => {
   beforeEach(() => {
      mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(200, authReturn.admin)
      mock.onGet(`${config.api.baseUrl}${config.api.routes.locations.mentors}`).reply(200, data.get)
      mock.onGet(`${config.api.baseUrl}${config.api.routes.mentors.available_add}`).reply(200, data.get)
      mock.onPost(`${config.api.baseUrl}${config.api.routes.mentors.add_mentor}`).reply(200, data.post)
      instance = mount(Mentors, { localVue })
      consoleSpy = jest.spyOn(global.console, 'error')
      toastSpy = jest.spyOn(instance.vm.$toasted, 'show')
   })

   afterEach(() => {
      instance.destroy()
      consoleSpy.mockReset()
      toastSpy.mockReset()
   })

   describe('fetchInitialData method', () => {
      it('should update data.campuses and data.mentorsAvailableToAdd with data from API', done => {
         expect.assertions(2)

         instance.vm.fetchInitialData().then(() => {
            expect(instance.vm.campuses).toEqual(data.get)
            expect(instance.vm.mentorsAvailableToAdd).toEqual(data.get)
            done()
         })
      })

      it('should set data.fetchedData to true after successful API request', done => {
         expect.assertions(1)

         instance.vm.fetchInitialData().then(() => {
            expect(instance.vm.fetchedData).toEqual(true)
            done()
         })
      })

      it('should use console.error and $toasted.show on a failed API request', done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.locations.mentors}`).reply(500, 'error')

         expect.assertions(2)
         // Use setTimeout because fetchInitialDate is called in the created lifecycle method
         setTimeout(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            expect(toastSpy.mock.calls.length).toEqual(1)
            done()
         }, 1)
      })

      it('should return null if user is not an admin', async done => {
         mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(200, authReturn.mentor)

         expect.assertions(1)

         const result = await instance.vm.fetchInitialData()

         expect(result).toEqual(null)
         done()
      })
   })

   describe('closeModal method', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.$options.methods.closeModal === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should set data.modalVisible to false', () => {
         instance.vm.$options.methods.closeModal()
         expect(instance.vm.modalVisible).toEqual(false)
      })
   })

   describe('addMentorToLocation method success', () => {
      it('should exist', () => {
         const methodExists = typeof instance.vm.addMentorToLocation === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should exit early and return null if capacity is <= 0', () => {
         let result

         result = instance.vm.addMentorToLocation(null, 0, null, null)
         expect(result).toEqual(null)

         result = instance.vm.addMentorToLocation(null, -4, null, null)
         expect(result).toEqual(null)
      })

      it('should call createMentor after hitting the API with two arguments', done => {
         const { mentor, capacity, dateStart, locationId } = fakeArguments

         instance.vm.createMentor = jest.fn()

         expect.assertions(3)

         instance.vm.addMentorToLocation(mentor, capacity, dateStart, locationId).then(r => {
            const mock = instance.vm.createMentor.mock
            const user = {
               id: data.post.id,
               user_id: data.post.user_id,
               first_name: mentor.first_name,
               last_name: mentor.last_name,
               capacity,
               active_students_count: 0
            }
            expect(mock.calls.length).toEqual(1)
            expect(mock.calls[0][0]).toEqual(user)
            expect(mock.calls[0][1]).toEqual(locationId)
            done()
         })
      })

      it('should use console.error and $toasted.show on a failed API request', done => {
         mock.onPost(`${config.api.baseUrl}${config.api.routes.mentors.add_mentor}`).reply(500, 'error')
         const { mentor, capacity, dateStart, locationId } = fakeArguments

         instance.vm.addMentorToLocation(mentor, capacity, dateStart, locationId).then(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            expect(toastSpy.mock.calls.length).toEqual(1)
            done()
         })
      })
   })

   describe('createMentor method', () => {
      let campuses
      let locationId = 'blah blah blah'

      beforeEach(() => {
         campuses = [{ id: 0, mentors: [] }, { id: locationId, mentors: [] }, { id: 2, mentors: [] }]
         mock.onGet(`${config.api.baseUrl}${config.api.routes.locations.mentors}`).reply(200, campuses)
         instance = mount(Mentors)
         instance.vm.campuses = campuses
      })

      it('should exist', () => {
         const methodExists = typeof instance.vm.createMentor === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should push a mentor to mentors array on data.campuses and set modalVisible to false', () => {
         const user = {
            id: 0,
            user_id: 1,
            first_name: 'first',
            last_name: 'last',
            capacity: 2,
            active_students_count: 0
         }

         instance.vm.createMentor(user, locationId)

         const mentorsArray = instance.vm.campuses[1].mentors

         expect(mentorsArray.length).toEqual(1)
         expect(mentorsArray[0]).toEqual(user)
         expect(instance.vm.modalVisible).toEqual(false)
      })
   })
})
