import StudentTable from '@/components/Cohorts/StudentTable/StudentTable.vue'
import { mount } from '@vue/test-utils'

describe('StudentTable.vue', () => {
   it('should render with the ColorLegend component', () => {
      let instance = mount(StudentTable, {
         propsData: {
            students: [],
            cohort: 'test'
         }
      })

      expect(instance.find({ ref: 'color-legend-component' }).exists()).toEqual(true)
   })
})
