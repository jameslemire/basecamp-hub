import Assign from '@/components/Landing/Modal/Menus/Assign'
import { mount } from '@vue/test-utils'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import config from '@/config'

let instance
let mockAxios = new MockAdapter(axios)

const fakeProps = {
   assignMentor: jest.fn(),
   borderType: 'blue-bottom-border',
   buttonType: 'blue-button',
   rowSelectedType: 'blue-selected-row',
   rowActionType: 'blue-action-row'
}

const fakeData = {
   mentors: [{id: 0, first: 'a0', last: 'b0'}, {id: 1, first: 'a1', last: 'b1'}, {id: 2, first: 'a2', last: 'b2'}],
   filterData: {
      selectedMentor: {}
   }
}

describe('Assign.vue', () => {
   beforeEach(() => {
      mockAxios.onGet(`${config.api.baseUrl}${config.api.routes.mentors.available_assign}`).reply(200, [])
      instance = mount(Assign, {
         propsData: fakeProps,
         data: fakeData
      })
   })

   afterEach(() => {
      instance.destroy()
   })

   it('should accept a borderType prop and render a header element with it as an ID', () => {
      const elementExists = instance.find('#blue-bottom-border').exists()
      expect(elementExists).toEqual(true)
   })

   it('should accept a buttonType prop and render a button element with it as an ID', () => {
      const elementExists = instance.find('#blue-button').exists()
      expect(elementExists).toEqual(true)
   })

   describe('created life cycle method', () => {
      let consoleSpy

      beforeEach(() => {
         consoleSpy = jest.spyOn(global.console, 'error')
         mockAxios.onGet(`${config.api.baseUrl}${config.api.routes.mentors.available_assign}`).reply(500, 'error')
         instance = mount(Assign, {
            propsData: fakeProps,
            data: fakeData
         })
      })

      afterEach(() => {
         consoleSpy.mockReset()
         instance.destroy()
      })

      it('should use console.error on a failed API request', done => {
         expect.assertions(1)

         setTimeout(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
            done()
         }, 1)
      })
   })
})
