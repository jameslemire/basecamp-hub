import Toasted from 'vue-toasted'
import Email from '@/components/Landing/Modal/Menus/Email'
import { mount, createLocalVue } from '@vue/test-utils'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import config from '@/config'

let instance
let mock = new MockAdapter(axios)

const fakeProps = {
   borderType: 'red-bottom-border',
   buttonType: 'red-button',
   student: {
      email: 'student1@testmail.net',
      full_name: 'Test Student'
   },
   closeModal: jest.fn()
}

const fakeData = {}

const localVue = createLocalVue()
localVue.use(Toasted)

describe('Email.vue', () => {
   beforeEach(() => {
      instance = mount(Email, {
         localVue,
         propsData: fakeProps,
         data: fakeData
      })
   })

   afterEach(() => {
      instance.destroy()
      fakeProps.closeModal.mockClear()
   })

   it('true should equal true', () => {
      expect(true).toEqual(true)
   })

   describe('sendWarningEmail method', () => {
      let consoleSpy
      let toastSpy

      beforeEach(() => {
         consoleSpy = jest.spyOn(global.console, 'error')
         toastSpy = jest.spyOn(instance.vm.$toasted, 'show')
      })

      afterEach(() => {
         consoleSpy.mockReset()
         toastSpy.mockReset()
      })

      it('should exist', () => {
         const methodExists = typeof instance.vm.sendWarningEmail === 'function'
         expect(methodExists).toEqual(true)
      })

      it('should use console.error on failed API request', () => {
         mock.onPost(`${config.api.baseUrl}${config.api.routes.email.warning}`).reply(500, 'err')

         expect.assertions(1)

         instance.vm.sendWarningEmail(fakeProps.student.email, fakeProps.student.full_name).then(() => {
            expect(consoleSpy.mock.calls.length).toEqual(1)
         })
      })

      it('should use $toasted.show and vm.closeModal on successful API request', done => {
         mock.onPost(`${config.api.baseUrl}${config.api.routes.email.warning}`).reply(200, {
            data: {
               from: 'basecamp@devmountain.com'
            }
         })

         expect.assertions(2)

         instance.vm.sendWarningEmail(fakeProps.student.email, fakeProps.student.full_name).then(() => {
            expect(toastSpy.mock.calls.length).toEqual(1)
            expect(fakeProps.closeModal.mock.calls.length).toEqual(1)
            done()
         })
      })
   })
})
