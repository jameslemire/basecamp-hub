import Menu from '@/components/Landing/Modal/Menu'
import { mount } from '@vue/test-utils'
import availableActions from '@/utils/availableActions'

let instance

let fakePropsAction = {
   closeModal: jest.fn(),
   assignMentor: jest.fn(),
   resolveAction: jest.fn(),
   selected: {
      student: {
         full_name: 'dummy name',
         enroll_date: '88/88/8888',
         start_date: '99/99/9999',
         action: 'unassigned',
         completion: '100%'
      },
      type: 'Action Item'
   },
   defaultedMenu: null
}

let fakePropsDanger = {
   closeModal: jest.fn(),
   assignMentor: jest.fn(),
   resolveAction: jest.fn(),
   selected: {
      student: {
         full_name: 'dummy name',
         enroll_date: '88/88/8888',
         start_date: '99/99/9999',
         action: 'unassigned',
         completion: '100%'
      },
      type: 'Danger Zone'
   },
   defaultedMenu: null
}

describe('Menu.vue', () => {
   describe('Both Menus', () => {
      beforeEach(() => {
         instance = mount(Menu, {
            propsData: fakePropsAction
         })
      })

      afterEach(() => {
         instance.destroy()
      })

      it('should render a button that can close the modal', () => {
         const buttonExists = instance.find({ ref: 'close-button' }).exists()
         expect(buttonExists).toEqual(true)
      })

      it('should call closeModal off of props when clicking the close button', () => {
         const button = instance.find({ ref: 'close-button' })
         button.trigger('click')
         expect(fakePropsAction.closeModal.mock.calls.length).toEqual(1)
      })

      it('should render options-menu to allow the user to select a menu', () => {
         const menuExists = instance.find({ ref: 'options-menu' }).exists()
         expect(menuExists).toEqual(true)
      })

      it('should render actions-menu to allow the user to select an action from a menu', () => {
         instance = mount(Menu, {
            propsData: Object.assign({}, fakePropsAction, { defaultedMenu: 'myAssign' })
         })

         const menuExists = instance.find({ ref: 'actions-menu' }).exists()
         expect(menuExists).toEqual(true)
      })
   })

   describe('Action Items Menu', () => {
      beforeEach(() => {
         instance = mount(Menu, {
            propsData: fakePropsAction
         })
      })

      afterEach(() => {
         instance.destroy()
      })

      it('should return the available menus for the unassigned action', () => {
         const actionItems = instance.vm.whichActions
         expect(actionItems).toEqual(availableActions.data().actionItems.unassigned)
      })

      it('should set colorType to blue', () => {
         const color = instance.vm.colorType
         expect(color).toEqual('blue')
      })
   })

   describe('Danger Zone Menu', () => {
      beforeEach(() => {
         instance = mount(Menu, {
            propsData: fakePropsDanger
         })
      })

      afterEach(() => {
         instance.destroy()
      })

      it('should return the available menus for the danger zone actions', () => {
         const dangerItems = instance.vm.whichActions
         expect(dangerItems).toEqual(availableActions.data().dangerZone)
      })

      it('should set colorType to red', () => {
         const color = instance.vm.colorType
         expect(color).toEqual('red')
      })
   })
})
