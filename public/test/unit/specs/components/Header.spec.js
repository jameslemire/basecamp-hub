import Header from '@/components/Header/Header.vue'
import { mount } from '@vue/test-utils'
import RouterConfig from '@/router/index'

describe('Header.vue', () => {
   it('should render successfully with 3 navigation links', () => {
      let instance = mount(Header, {
         router: RouterConfig
      })

      expect(instance.findAll('a').length).toEqual(3)

      instance.destroy()
   })
})
