import availableActions, { assignMentor, resolveAction, emailStudent } from '@/utils/availableActions.js'
import Email from '@/components/Landing/Modal/Menus//Email'
import Assign from '@/components/Landing/Modal/Menus//Assign'
import Resolve from '@/components/Landing/Modal/Menus//Resolve'

describe('availableActions.js', () => {
   it('should return an object', () => {
      expect(typeof availableActions).toEqual('object')
   })

   it('should return a data function', () => {
      expect(typeof availableActions.data).toEqual('function')
   })

   it('should have an actionItems and dangerZone property on data', () => {
      const data = availableActions.data()
      expect(data.actionItems).toBeDefined()
      expect(data.dangerZone).toBeDefined()
   })

   it('should assign the correct menus to the different action types', () => {
      const data = availableActions.data()

      expect(data.actionItems.general).toEqual([ resolveAction ])
      expect(data.actionItems.deferred).toEqual([ assignMentor ])
      expect(data.actionItems.unassigned).toEqual([ assignMentor ])
   })

   it('should assign the correct menus to danger zone students', () => {
      const data = availableActions.data()

      expect(data.dangerZone).toEqual([ emailStudent ])
   })

   it('should assign components to use for menus', () => {
      const components = availableActions.components

      expect(components).toBeDefined()
      expect(components.myEmail).toEqual(Email)
      expect(components.myAssign).toEqual(Assign)
      expect(components.myResolve).toEqual(Resolve)
   })
})
