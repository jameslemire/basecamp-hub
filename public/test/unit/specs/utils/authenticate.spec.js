import authFn from '@/utils/authenticate'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import config from '@/config'
import authRetun from '../components/auth_return'

const mock = new MockAdapter(axios)

describe('authenticate.js', () => {
   it('should use console.error on a failed authentication', done => {
      const consoleSpy = jest.spyOn(global.console, 'error')
      window.location.assign = jest.fn()
      mock.onGet(`${config.api.baseUrl}${config.api.routes.auth.check}`).reply(500, 'error')

      expect.assertions(1)

      authFn().then(() => {
         expect(consoleSpy.mock.calls.length).toEqual(1)
         consoleSpy.mockReset()
         done()
      })
   })
})
