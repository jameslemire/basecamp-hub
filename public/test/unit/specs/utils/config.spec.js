import { generateApiConfig } from '@/config.js'

describe('Config.js', () => {
   it('should set a development API base url', () => {
      process.env.NODE_ENV = 'development'
      const config = generateApiConfig()
      expect(config.api.baseUrl).toEqual('http://localhost:4000')
   })

   it('should set a development API base url', () => {
      process.env.NODE_ENV = 'test'
      const config = generateApiConfig()
      expect(config.api.baseUrl).toEqual('http://localhost:4000')
   })

   it('should set a production API base url', () => {
      process.env.NODE_ENV = 'production'
      const config = generateApiConfig()
      expect(config.api.baseUrl).toEqual('http://104.236.251.233')
   })

   it('should use console.error if NODE_ENV is unknown and set baseUrl to "unknown"', () => {
      const spy = jest.spyOn(global.console, 'error')
      process.env.NODE_ENV = 'asfsdgsdfg'
      const config = generateApiConfig()
      expect(config.api.baseUrl).toEqual('unknown')
      expect(spy.mock.calls.length).toEqual(1)
      spy.mockReset()
   })
})
