#!/bin/sh

# cd ~/basecamp-hub
# git pull

docker rm -f basecamp-hub-server-container
docker rm -f basecamp-hub-redis-container
docker-compose -f docker-compose-prod.yml up -d --build
docker container prune -f
docker image prune -f
