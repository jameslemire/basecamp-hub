module.exports = {
  withMentorRole: function() {
    return `SELECT u.id as user_id, u.first_name, u.last_name
            FROM "user" u
            JOIN user_roles__userrole_users ur ON ur.user_roles = u.id
            JOIN userrole r ON r.id = ur.userrole_users
            WHERE r.role = 'mentor'
            AND u.id NOT IN (SELECT user_id FROM bc_mentor)`
  }
}