function serializeDate(date) {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

module.exports = {
  oneWeekAgo: () => {
    let date = new Date();
    date.setDate(date.getDate() - 7);
    return serializeDate(date);
  },

  threeMonthsFromNow: () => {
    let date = new Date();
    date.setMonth(date.getMonth() + 3);
    return serializeDate(date);
  },

  sixWeeksFromNow: () => {
    let date = new Date();
    date.setDate(date.getDate() + 42);
    return serializeDate(date);
  },

  tomorrow: () => {
    let date = new Date();
    date.setDate(date.getDate() + 1);
    return serializeDate(date);
  },

  isNumber: (n) => {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
}