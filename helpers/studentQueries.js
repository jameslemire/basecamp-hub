const { oneWeekAgo, threeMonthsFromNow, isNumber } = require('./shared');

module.exports = {
  deferredStudents: function() {
    return `SELECT u.first_name, u.last_name, e.id as enrollment_id, e.created_at as date_enrolled, c.date_start as cohort_start_date, e.session_id, c.short_name as cohort_name, me.session_id as original_session_id
            FROM enrollment e
            JOIN "user" u ON e.user_id = u.id
            JOIN classsession c ON e.session_id = c.id
            JOIN mentor_enrollment me ON me.enrollment_id = e.id
            WHERE e.status = 'Admitted'
            AND e.id IN (SELECT enrollment_id FROM mentor_enrollment)
            AND me.is_active = true
            AND c.subject = 'webdev'
            AND e.session_id != me.session_id`;
  },

  withoutMentor: function() {
    return `SELECT u.first_name, u.last_name, e.id as enrollment_id, e.created_at as date_enrolled, c.date_start as cohort_start_date, e.session_id, c.short_name as cohort_name
            FROM enrollment e
            JOIN "user" u ON e.user_id = u.id
            JOIN classsession c ON e.session_id = c.id
            LEFT JOIN mentor_enrollment me ON e.id = me.enrollment_id
            WHERE c.date_start BETWEEN '${oneWeekAgo()}' AND '${threeMonthsFromNow()}'
            AND e.status = 'Admitted'
            AND c.is_public = true
            AND subject = 'webdev'
            AND me.mentor_id IS NULL`;
  },

  droppedStudents: function() {
    return `SELECT u.first_name, u.last_name, e.id as enrollment_id, e.created_at as date_enrolled, c.date_start as cohort_start_date, e.session_id, c.short_name as cohort_name
            FROM enrollment e
            JOIN "user" u ON e.user_id = u.id
            JOIN classsession c ON e.session_id = c.id
            WHERE c.date_start BETWEEN '${oneWeekAgo()}' AND '${threeMonthsFromNow()}'
            AND e.status IN ('Withdrawn - No Deposit', 'Withdrawn', 'Dropped')
            AND c.is_public = true
            AND subject = 'webdev'`
  },

  flaggedStudents: function() {
    return `SELECT u.first_name, u.last_name, e.id as enrollment_id, e.created_at as date_enrolled, c.date_start as cohort_start_date, e.session_id, c.short_name as cohort_name, t.meta, t.id as trackable_id
            FROM enrollment e
            JOIN "user" u ON e.user_id = u.id
            JOIN classsession c ON e.session_id = c.id
            JOIN trackable t ON t.enrollment_id = e.id
            JOIN trackable_type tt ON tt.id = t.type_id
            WHERE tt.name = 'flag'
            AND c.date_start BETWEEN '${oneWeekAgo()}' AND '${threeMonthsFromNow()}'
            AND t.meta->'resolved' = 'false'
            AND c.is_public = true
            AND subject = 'webdev'`
  },

  studentsForMentor: function(id) {
    return `SELECT e.id as enrollment_id, u.first_name, u.last_name, c.date_start as cohort_start_date
            FROM enrollment e
            JOIN mentor_enrollment me ON e.id = me.enrollment_id
            JOIN bc_mentor m ON me.mentor_id = m.id
            JOIN classsession c ON c.id = e.session_id
            JOIN "user" u ON e.user_id = u.id
            WHERE m.user_id = ${id}
            AND me.is_active = true`
  }
}
