const AuthenticationController = require('./AuthenticationController');
const MentorsController = require('./MentorsController');
const StudentsController = require('./StudentsController');
const TrackablesController = require('./TrackablesController');
const LocationsController = require('./LocationsController');
const UsersController = require('./UsersController');
const EmailController = require('./EmailController');
// used to sync data from mysql db
const EnrollmentController = require('./EnrollmentController');
const SessionController = require('./SessionController');
const UserRoleController = require('./UserRoleController');
const UserRoleUserController = require('./UserRoleUserController');

module.exports = { 
  AuthenticationController, 
  MentorsController, 
  StudentsController, 
  TrackablesController, 
  LocationsController,
  UsersController,
  EnrollmentController,
  SessionController,
  UserRoleController,
  UserRoleUserController,
  EmailController
 };