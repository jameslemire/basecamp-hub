const Op = require('sequelize').Op;
const { Mentor, Enrollment, User, Location, Session } = require('../models');
const LocationSerializer = require('../serializers/Location');

module.exports = {
  locationsWithMentorInfo: async (req, res) => {
    try {
      let locations = await Location.withMentorInfo();
      let serializedLocations = LocationSerializer.serializeWithMentors(locations);
      res.send(serializedLocations)
    } catch (error) {
      console.error('error fetching locations:', error);
      res.status(500).send(error);
    }
  },

  locationsWithCohortInfo: async (req, res) => {
    try {
      let locations = await Location.withCohortInfo();
      serializedLocations = await LocationSerializer.serializeWithCohorts(locations);
      res.send(serializedLocations)
    } catch (error) {
      console.error('error fetching locations:', error);
      res.status(500).send(error);
    }
  }
}
