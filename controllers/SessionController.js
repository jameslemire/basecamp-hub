const { Session } = require('../models');

module.exports = {
  create: async (req, res) => {
    try {
      let session = await Session.create(req.body);
      res.send(session);
    } catch (err) {
      console.error('err while creating new session:', err);
      res.status(500).send(err);
    }
  },

  update: async (req, res) => {
    try {
      let session = await Session.update(req.body, {where: {id: req.params.id}});
      res.send(session);
    } catch (err) {
      console.error('err while updating session:', err);
      res.status(500).send(err);
    }
  }
}