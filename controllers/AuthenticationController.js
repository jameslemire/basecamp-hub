const Devmtn = require('devmtn-auth')

module.exports = {
   logout: (req, res) => {
      req.logout()
      res.redirect('/auth/devmtn')
   },

   clearJwtAuthCookie: Devmtn.clearJwtAuthCookie,

   loginSuccessRouter: (req, res) => {
      console.log('Login Success, NODE_ENV:', process.env.NODE_ENV)
      if (process.env.NODE_ENV === 'development') {
         return res.redirect('http://localhost:8080/')
      }
      res.redirect('/')
   }
}
