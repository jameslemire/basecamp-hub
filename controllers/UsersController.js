const { User } = require('../models');

module.exports = {
  withMentorRole: async (req, res) => {
    try {
      let response = await User.withMentorRole();
      res.send(response);
    } catch (error) {
      console.error('error fetching users with mentor role:', error);
      res.status(500).send(error);
    }
  },

  create: async (req, res) => {
    try {
      let user = await User.create(req.body);
      res.send(user);
    } catch (err) {
      console.error('err while creating new user:', err);
      res.status(500).send(err);
    }
  },

  update: async (req, res) => {
    try {
      let user = await User.update(req.body, {where: {id: req.params.id}});
      res.send(user);
    } catch (err) {
      console.error('err while updating user:', err);
      res.status(500).send(err);
    }
  }
}