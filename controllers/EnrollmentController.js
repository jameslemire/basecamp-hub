const { Enrollment, Trackable, TrackableType } = require('../models');

module.exports = {
  create: async (req, res) => {
    try {
      let enrollment = await Enrollment.create(req.body);
      res.send(enrollment);
    } catch (err) {
      console.error('err while creating new enrollment:', err);
      res.status(500).send(err);
    }
  },

  update: async (req, res) => {
    const { id } = req.params;
    const updatedEnrollment = req.body;
    const droppedStatuses = ['Withdrawn - No Deposit', 'Withdrawn', 'Dropped'];

    try {
      if (droppedStatuses.indexOf(updatedEnrollment.status) === -1 ) {
        let enrollment = await Enrollment.update(req.body, { where: { id } });
        return res.send(enrollment);
      } else {
        // updated enrollment has a dropped status
        let enrollment = await Enrollment.findOne({ where: { id } });
        if (droppedStatuses.indexOf(enrollment.status) === -1) {
          // original enrollment was not dropped so we need to create trackable with dropped flag
          let type = await TrackableType.findOne({ where: { name: 'flag' } });
          await Trackable.create({
            enrollment_id: enrollment.id,
            type_id: type.id,
            updated_by_user_id: req.user ? res.user.id : 884, //TODO: change this once auth is working
            meta: {
              flagType: 'dropped',
              resolved: false
            }
          });
          let update = await enrollment.update(updatedEnrollment);
          return res.send(update);
        } else {
          let update = await enrollment.update(updatedEnrollment);
          return res.send(update);
        }
        res.send(enrollment);
      }
    } catch (err) {
      console.error('err while updating enrollment:', err);
      res.status(500).send(err);
    }
  }
}