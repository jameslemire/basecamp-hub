const { UserRoleUser } = require('../models');

module.exports = {
  create: async (req, res) => {
    const { user_id } = req.body;
    await UserRoleUser.destroy({ where: { user_roles: user_id } });
    await req.body.roles.forEach(async role => {
      let body = {
        user_roles: user_id,
        userrole_users: role.id
      }

      await UserRoleUser.upsert(body);
    });

    res.send('ok')
  }
}