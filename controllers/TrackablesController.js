const {Enrollment, Mentor, MentorEnrollment, Trackable, TrackableType, User} = require('../models');

module.exports = {
  flagged: async (req, res) => {
    try {
      let response = await Trackable.unresolvedFlags()
      res.send(response);
    } catch (error) {
      console.error('error fetching flagged trackables:', error);
      res.status(500).send(error);
    }
  },

  resolveFlaggedItem: async (req, res) => {
    const { id } = req.params;
  
    try {
      let trackable = await Trackable.findOne({ where:{id} });
      trackable.set({ 
        'meta.resolved': true,
        updated_by_user_id: req.user ? req.user.id : 1350 //TODO: take out the 1350 once auth is working
       });

      try {
        let response = await trackable.save();
        return res.send(response);
      } catch(err) {
        console.error(err);
        return res.status(500).send(err);
      }
    } catch(err) {
      console.error(err);
      return res.status(500).send(err);
    }
  },

  post: async (req, res) => {
    try {
      let trackable = await Trackable.create(req.body);
      res.send(trackable);
    } catch (error) {
      console.error('error creating new trackable:', error);
      res.status(500).send(error)
    }
  },

  interaction: async (req, res) => {
    try {
      const { enrollment_id } = req.params;
      const { completion, confidence, notes } = req.body;
      const updated_by_user_id = req.user.id;
      const trackable_type = await TrackableType.findOne({ where: { name: 'bc_interaction' } });
      const type_id = trackable_type.id;
      
      let newTrackable = { enrollment_id, updated_by_user_id, type_id, meta: { completion, confidence, notes} };
      let trackable = await Trackable.create(newTrackable);
      res.send(trackable)
    } catch (error) {
      console.error('error creating interaction trackable:', error);
    }
  }
}