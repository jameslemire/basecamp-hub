const {Enrollment, Mentor, MentorEnrollment, Trackable, TrackableType, User} = require('../models');

module.exports = {
  index: (req, res) => {
    // Enrollment.activeBasecampStudents().then(result => {
    //   res.send(result)
    // })

    res.send('still working on this')
  },

  show: (req, res) => {

  },

  put: (req, res) => {

  },

  post: async (req, res) => {
    try {
      let enrollment = await Enrollment.create(req.body);
      res.send(enrollment);
    } catch (error) {
      console.error('error creating new enrollment:', error)
      return res.status(500).send(error);
    }
  },

  delete: (req, res) => {

  },

  actionRequired: async (req, res) => {
    try {
      let deferred = await Enrollment.deferred();
      let flagged = await Enrollment.flagged();
      let unassigned = await Enrollment.withoutMentor();

      deferred = deferred.map(student => {
        return { ...student, action: 'deferred', label: 'Deferred' };
      });

      unassigned = unassigned.map(student => {
        return { ...student, action: 'unassigned', label: 'Unassigned' };
      });

      flagged = flagged.map(student => {
        return { ...student, action: student.meta.flagType, label: student.meta.flagType }
      })

      res.send([ ...deferred, ...flagged, ...unassigned ]);
    } catch (error) {
      console.error(error);
      res.status(500).send(error);
    }
  },

  deferred: async (req, res) => {
    try {
      let deferredStudents = await Enrollment.deferred();
      return res.send(deferredStudents);
    } catch(err) {
      console.error('error fetching deferred students:', err);
      return res.status(500).send(err);
    }
  },

  droppedStudents: async (req, res) => {
    try {
      let droppedStudents = await Enrollment.dropped();
      return res.send(droppedStudents);
    } catch(err) {
      console.error('error fetching dropped students:', err);
      return res.status(500).send(err);
    }
  },

  flaggedStudents: async (req, res) => {
    try {
      let flaggedStudents = await Enrollment.flagged();
      return res.send(flaggedStudents);
    } catch (error) {
      console.log('error fetching flagged students:', error);
      return res.status(500).send(error);
    }
  },

  withoutMentor: async (req, res) => {
    try {
      let studentsWithoutMentor = await Enrollment.withoutMentor();
      return res.send(studentsWithoutMentor);
    } catch (error) {
      console.error('error fetching students without mentor:', error);
      res.status(500).send(error);
    }
  },

  dangerZoneStudents: async (req, res) => {
    try {
      let activeStudents = await Enrollment.activeStudents();
      let dangerZoneStudents = activeStudents.filter(student => {
        return Enrollment.isStudentInDanger(student)
      })
      return res.send(dangerZoneStudents);
    } catch (error) {
      console.error('error fetching danger zone students:', error);
      res.status(500).send(error);
    }
  },
  
  assignMentor: async (req, res) => {
    const { enrollment_id, mentor_id, session_id } = req.body;

    try {
      await MentorEnrollment.update({
        is_active: false
      }, {
        where: { enrollment_id }
      });

      try {
        let response = await MentorEnrollment.create({ enrollment_id, mentor_id, session_id, is_active: true });
        return res.send(response);
      } catch (err) {
        console.error(err);
        res.status(500).send(err);
      }
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
}