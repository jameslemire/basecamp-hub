const { EmailTemplate } = require('../models');
const EmailService = require('../services/EmailService');
const { isNumber } = require('../helpers/shared');

module.exports = {
  create: async (req, res) => {
    try {
      const { name, text, from, to, variables } = req.body
      const result = await EmailTemplate.create({ name, text, from, to, variables });
      res.send(result);
    } catch (error) {
      console.log('error creating new email template:', error);
      res.status(500).send(error);
    }
  },

  sendEmail: async (req, res) => {
    let { templateId } = req.params;
    let field = isNumber(templateId) ? 'id' : 'name'

    const emailTemplate = await EmailTemplate.findOne({ where: { [field]: templateId } });
    let { text, variables, from, subject } = emailTemplate;
    variables = variables.split(', ');

    variables.forEach(v => {
      text = text.split(`#[${v}]`).join(req.body[v])
    })

    const { studentName, studentEmail } = req.body;
    const to = `${studentName} <${studentEmail}>`;

    let result = await EmailService.send({ text, to, from, subject })
    res.send(result);
  }
}