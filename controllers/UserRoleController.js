const { UserRole } = require('../models');

module.exports = {
  create: async (req, res) => {
    try {
      let role = await UserRole.create(req.body);
      res.send(role);
    } catch (err) {
      console.error('err while creating new role:', err);
      res.status(500).send(err);
    }
  },

  update: async (req, res) => {
    try {
      let role = await UserRole.update(req.body, {where: {id: req.body.id}});
      res.send(role);
    } catch (err) {
      console.error('err while updating role:', err);
      res.status(500).send(err);
    }
  }
}