const { Mentor, Enrollment, User, MentorEnrollment, Location } = require('../models');
const MentorSerializer = require('../serializers/Mentor');

module.exports = {
  index: async (req, res) => {
    try {
      let mentors = await Mentor.findAll({
        attributes: ['id', 'capacity'],
        include: [
          {
            model: User,
            attributes: ['first_name', 'last_name', 'phone', 'email']
          },
          {
            model: Location,
            attributes: ['id', 'name']
          },
          {
            model: Enrollment,
            attributes: ['id'],
            include: [
              { 
                model: User, attributes: ['first_name', 'last_name', 'phone', 'email'],
              },
              {
                model: MentorEnrollment,
                where: {is_active: true}
              }
            ]
          }
        ]
      })

      return res.send(mentors);
    } catch (error) {
      res.status(500).send(error);
    }
  },

  available: async(req, res) => {
    try {
      let mentors = await Mentor.available();
      let serializedMentors = MentorSerializer.serialize(mentors);
      res.send(serializedMentors);
    } catch (error) {
      console.error('error fetching available mentors:', error);
      res.status(500).send(error);
    }
  },

  show: async (req, res) => {
    const { id } = req.params
    try {
      let mentor = await Mentor.findBy({ id });
      res.send(mentor);
    } catch (error) {
      console.error('error getting mentor:', error);
      res.status(500).send(error);
    }
  },

  getStudentsForMentor: async (req, res) => {
    const { user_id } = req.params;
    try {
      let students = await Mentor.getStudentsForMentor(user_id);
      res.send(students);
    } catch (error) {
      console.error('error getting mentor\'s students:', error);
      res.status(500).send(error);
    }
  },

  post: async (req, res) => {
    try {
      let response = await Mentor.create(req.body);
      res.send(response);
    } catch (error) {
      console.error('error creating mentor record:', error);
      res.status(400).send('Could not create mentor');
    }
  },

  delete: (req, res) => {

  }
}