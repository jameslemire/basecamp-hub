module.exports = (sequelize, dataTypes) => {
  const TrackableType = sequelize.define('TrackableType', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: dataTypes.STRING
    },

  }, {
    tableName: 'trackable_type',
    underscored: true
  })

  return TrackableType
}