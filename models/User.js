const queries = require('../helpers/userQueries');

module.exports = (sequelize, dataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    email: {
      type: dataTypes.STRING,
      validate: {
        isEmail: true
      }
    },

    password: {
      type: dataTypes.STRING
    },

    first_name: {
      type: dataTypes.STRING
    },

    last_name: {
      type: dataTypes.STRING
    },

    birthday: {
      type: dataTypes.DATE
    },

    phone: {
      type: dataTypes.STRING
    },

    gender: {
      type: dataTypes.STRING
    },

    address: {
      type: dataTypes.STRING
    },

    city: {
      type: dataTypes.STRING
    },

    state: {
      type: dataTypes.STRING
    },

    country: {
      type: dataTypes.STRING
    },

    zip: {
      type: dataTypes.STRING
    },

    github_id: {
      type: dataTypes.STRING
    },

    github_token: {
      type: dataTypes.STRING
    },

    linkedin_id: {
      type: dataTypes.STRING
    },

    linkedin_token: {
      type: dataTypes.STRING
    },

    sf_portal: {
      type: dataTypes.BOOLEAN
    }
  }, {
    tableName: 'user',
    underscored: true,
    getterMethods: {
      fullName: () => {}
    }
  });

  User.associate = function (models) {
    User.hasMany(models.UserRoleUser, {foreignKey: 'user_roles'});
    User.belongsToMany(models.UserRole, {through: 'UserRoleUser', foreignKey: 'user_roles'});
  }

  User.withMentorRole = function() {
    return sequelize.query(queries.withMentorRole(), { type: sequelize.QueryTypes.SELECT });
  }

  return User
}