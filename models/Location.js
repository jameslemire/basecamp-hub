const Op = require('sequelize').Op
const { oneWeekAgo, threeMonthsFromNow } = require('../helpers/shared');

module.exports = (sequelize, dataTypes) => {
  const Location = sequelize.define('Location', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: dataTypes.STRING
    },

    short_name: {
      type: dataTypes.STRING
    },

    city: {
      type: dataTypes.STRING
    },

    state: {
      type: dataTypes.STRING
    }
    
  }, {
    tableName: 'location',
    underscored: true
  })

  Location.associate = function (models) {
    Location.hasMany(models.Mentor)
    Location.hasMany(models.Session)
  }

  Location.withMentorInfo = async () => {
    return await Location.findAll({
      attributes: ['id', 'name'],
      include: [
        {
          association: 'Sessions',
          attributes: [],
          where: {
            subject: 'webdev',
            date_start: {
              [Op.between]: [oneWeekAgo(), threeMonthsFromNow()]
            }
          }
        }, 
        {
          association: 'Mentors',
          attributes: ['id', 'user_id', 'capacity'],
          include: [
            {
              association: 'User',
              attributes: ['email', 'phone', 'first_name', 'last_name']
            },
            {
              association: 'Enrollments',
              attributes: ['id']
            }
          ]
        }
      ]
    });
  }

  Location.withCohortInfo = async () => {
    return await Location.findAll({
      attributes:['id', 'name'],
      include: [
        {
          association: 'Sessions',
          attributes: ['id', 'short_name'],
          include: [
            {
              association: 'Enrollments',
              attributes: ['id'],
              include: [
                {
                  association: 'User',
                  attributes: ['first_name', 'last_name', 'email', 'phone']
                },
                {
                  association: 'Mentors',
                  attributes: ['id'],
                  include: [
                    {
                      association: 'User',
                      attributes: ['first_name', 'last_name', 'id']
                    }
                  ]
                }
              ]
            }
          ],
          where: {
            subject: 'webdev',
            date_start: {
              [Op.between]: [oneWeekAgo(), threeMonthsFromNow()]
            }
          }
        }
      ]
    })
  }

  return Location
}