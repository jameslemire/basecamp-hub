const { isNumber } = require('../helpers/shared');
const { studentsForMentor } = require('../helpers/studentQueries');

module.exports = (sequelize, dataTypes) => {
  const Mentor = sequelize.define('Mentor', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    user_id: {
      type: dataTypes.INTEGER
    },

    capacity: {
      type: dataTypes.INTEGER
    },

    date_start: {
      type: dataTypes.DATE
    },

    date_end: {
      type: dataTypes.DATE
    },
    created_at: {
      type: dataTypes.DATE,
      allowNull: false
    },

    updated_at:  {
      type: dataTypes.DATE
    },

    deleted_at: {
      type: dataTypes.DATE
    }

  }, {
    tableName: 'bc_mentor',
    underscored: true
  })

  Mentor.associate = function (models) {
    Mentor.belongsTo(models.User, {foreignKey: 'user_id'});
    Mentor.belongsTo(models.Location, {foreignKey: 'location_id'})
    Mentor.hasMany(models.MentorEnrollment);
    Mentor.belongsToMany(models.Enrollment, {through: 'MentorEnrollment', foreignKey: 'mentor_id'})

    Mentor.prototype.activeStudents = function() {
      let mentorId = this.id;
      let query = `SELECT COUNT(*) FROM mentor_enrollment
                   WHERE mentor_id = ${mentorId}
                   AND is_active = true`;
      return sequelize.query(query, { type: sequelize.QueryTypes.SELECT })
    }

    Mentor.available = function() {
      let { or, lte, gt, eq } = sequelize.Op;
      
      return Mentor.findAll({
        attributes: ['id', 'user_id', 'capacity'],
        include: [
          {
            association: 'User',
            attributes: ['first_name', 'last_name', 'phone', 'email']
          }, 
          {
            association: 'Enrollments',
            attributes: ['id']
          },
          {
            association: 'Location',
            attributes: ['id', 'name']
          }
        ],
        where: {
          date_start: {
            [lte]: new Date() 
          },
          date_end: {
            [or]: {
              [gt]: new Date(),
              [eq]: null
            }
          }
        }
      })
    }

    Mentor.getStudentsForMentor = function(userId) {
      if (!isNumber(userId)) throw 'The value given to me is not a number and therefore I will not execute this query.  You know...for security reasons!'
      return sequelize.query(studentsForMentor(userId), { type: sequelize.QueryTypes.SELECT });
    }
  }

  return Mentor
}