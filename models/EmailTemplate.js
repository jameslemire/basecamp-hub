module.exports = (sequelize, dataTypes) => {
  const EmailTemplate = sequelize.define('EmailTemplate', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    name: {
      type: dataTypes.STRING
    },

    subject: {
      type: dataTypes.STRING
    },

    text: {
      type: dataTypes.TEXT
    },
    
    from: {
      type: dataTypes.STRING
    },

    to: {
      type: dataTypes.STRING
    },

    variables: {
      type: dataTypes.STRING
    },
    
  }, {
    tableName: 'email_template',
    underscored: true
  })

  return EmailTemplate
}