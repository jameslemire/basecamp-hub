const Op = require('sequelize').Op

module.exports = (sequelize, dataTypes) => {
  const UserRoleUser = sequelize.define('UserRoleUser', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    user_roles: {
      type: dataTypes.INTEGER
    },

    userrole_users: {
      type: dataTypes.INTEGER
    }
    
  }, {
    tableName: 'user_roles__userrole_users',
    underscored: true,
    timestamps: false
  })

  UserRoleUser.associate = function (models) {
    UserRoleUser.belongsTo(models.User, {foreignKey: 'user_roles'});
    UserRoleUser.belongsTo(models.UserRole, {foreignKey: 'userrole_users'});
  }

  return UserRoleUser
}