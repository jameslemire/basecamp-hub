module.exports = (sequelize, dataTypes) => {
  const MentorEnrollment = sequelize.define('MentorEnrollment', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    mentor_id: {
      type: dataTypes.INTEGER
    },

    enrollment_id: {
      type: dataTypes.INTEGER
    },

    is_active: {
      type: dataTypes.BOOLEAN
    },

    session_id: {
      type: dataTypes.INTEGER
    },

  }, {
    tableName: 'mentor_enrollment',
    underscored: true
  })

  MentorEnrollment.associate = function (models) {
    MentorEnrollment.belongsTo(models.Mentor, {foreignKey: 'mentor_id'});
    MentorEnrollment.belongsTo(models.Enrollment, {foreignKey: 'enrollment_id'});
  }

  return MentorEnrollment
}