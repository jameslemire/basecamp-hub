const Op = require('sequelize').Op
const queries = require('../helpers/studentQueries');
const { tomorrow, sixWeeksFromNow } = require('../helpers/shared');
const StudentSerializer = require('../serializers/Student');

module.exports = (sequelize, dataTypes) => {
  const Enrollment = sequelize.define('Enrollment', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    session_id: {
      type: dataTypes.INTEGER
    },

    user_id: {
      type: dataTypes.INTEGER
    },

    deposit: {
      type: dataTypes.INTEGER
    },

    price: {
      type: dataTypes.INTEGER
    },

    has_paid_deposit: {
      type: dataTypes.BOOLEAN
    },

    amount_paid_total: {
      type: dataTypes.INTEGER
    },

    mentor: {
      type: dataTypes.INTEGER
    },

    status: {
      type: dataTypes.STRING
    },

    has_paid_parking_pass: {
      type: dataTypes.BOOLEAN
    },

    has_paid_transit_pass: {
      type: dataTypes.BOOLEAN
    },

    has_paid_gym_pass: {
      type: dataTypes.BOOLEAN
    },

    package_sent: {
      type: dataTypes.BOOLEAN
    },

    has_paid_housing_deposit: {
      type: dataTypes.BOOLEAN
    },

    has_paid_private_room_deposit: {
      type: dataTypes.BOOLEAN
    },

    graduation_date: {
      type: dataTypes.DATE
    },

    start_date: {
      type: dataTypes.DATE
    },

    sf_id: {
      type: dataTypes.STRING
    },
    
  }, {
    tableName: 'enrollment',
    underscored: true
  })

  Enrollment.associate = function (models) {
    Enrollment.belongsTo(models.User, {foreignKey: 'user_id'})
    Enrollment.belongsTo(models.Session, {foreignKey: 'session_id'})
    Enrollment.hasMany(models.MentorEnrollment);
    Enrollment.belongsToMany(models.Mentor, {through: 'MentorEnrollment', foreignKey: 'enrollment_id'})
    Enrollment.hasMany(models.Trackable, {foreignKey: 'enrollment_id'})

    Enrollment.withoutMentor = async function() {
      return sequelize.query(queries.withoutMentor(), { type: sequelize.QueryTypes.SELECT });
    }

    Enrollment.deferred = function() {
      return sequelize.query(queries.deferredStudents(), { type: sequelize.QueryTypes.SELECT });
    }

    Enrollment.dropped = function() {
      return sequelize.query(queries.droppedStudents(), { type: sequelize.QueryTypes.SELECT });
    }

    Enrollment.flagged = function() {
      return sequelize.query(queries.flaggedStudents(), { type: sequelize.QueryTypes.SELECT });
    }

    Enrollment.activeStudents = async function() {
      const trackableType = await models.TrackableType.findOne({ where: { name: 'bc_interaction' } });
      const type_id = trackableType.id;
      const students = await Enrollment.findAll({
        attributes: ['id'],
        where: { status: 'Admitted' },
        include: [
          {
            model: models.User,
            attributes: ['first_name', 'last_name', 'email', 'phone']
          }, 
          {
          model: models.Session,
            attributes: ['short_name', 'date_start', 'date_end'],
            where: {
              subject: 'webdev',
              date_start: {
                [Op.between]: [tomorrow(), sixWeeksFromNow()]
              }
            }
          }, 
          {
            model: models.Trackable,
            where: { type_id },
            order: [['created_at', 'DESC']],
            limit: 1
          }
        ]
      });

      return StudentSerializer.serialize(students);
    }

    Enrollment.isStudentInDanger = function(student) {
      if (!student.last_interaction) return false;

      const { completion } = student.last_interaction.meta;
      const cohort_start_date = new Date(student.cohort_start_date);
      const interaction_date = new Date(student.last_interaction.created_at);
      const diffInWeeks = Math.floor((cohort_start_date - interaction_date)/(7*24*60*60*1000));

      switch (diffInWeeks) {
        case 5:
          return completion <= 13;
        case 4: 
          return completion <= 26;
        case 3:
          return completion <= 39;
        case 2:
          return completion <= 52;
        case 1: 
          return completion <= 65;
        case 0:
          return completion <= 78;
        default:
          return false;
      }
    }

    Enrollment.prototype.getMostRecentInteraction = async function() {
      const trackableType = await models.TrackableType.findOne({ where: { name: 'bc_interaction' } });
      const type_id = trackableType.id;
      const enrollment_id = this.id;
      let response = { meta: { confidence: 0, completion: 0 } };

      const trackable = await models.Trackable.findOne({ 
        where: { type_id, enrollment_id }, 
        order: [['created_at', 'DESC']] 
      });

      if (trackable) response = trackable;

      return response
    }
  }

  return Enrollment
}
