const Op = require('sequelize').Op

module.exports = (sequelize, dataTypes) => {
  const UserRole = sequelize.define('UserRole', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    role: {
      type: dataTypes.STRING
    }
    
  }, {
    tableName: 'userrole',
    underscored: true
  })

  UserRole.associate = function (models) {
    UserRole.hasMany(models.UserRoleUser, {foreignKey: 'userrole_users'});
    UserRole.belongsToMany(models.User, {through: 'UserRoleUser', foreignKey: 'userrole_users'});
  }

  return UserRole
}