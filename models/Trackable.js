module.exports = (sequelize, dataTypes) => {

  const Trackable = sequelize.define('Trackable', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    enrollment_id: {
      type: dataTypes.INTEGER
    },

    type_id: {
      type: dataTypes.INTEGER
    },

    updated_by_user_id: {
      type: dataTypes.INTEGER 
    },

    updated_at: {
      type: dataTypes.DATE //TODO: make sure is timestamp
    },

    created_at: {
      type: dataTypes.DATE //TODO: make sure is timestamp
    },

    meta: {
      type: dataTypes.JSONB, //meta example { flagType, notes, resolved }
    }

  }, {
    tableName: 'trackable',
    underscored: true
  })

  Trackable.associate = function (models) {
    Trackable.belongsTo(models.TrackableType, {foreignKey: 'type_id'});
    Trackable.belongsTo(models.User, {foreignKey: 'updated_by_user_id'});
    Trackable.belongsTo(models.Enrollment, {foreignKey: 'enrollment_id'});

    
    Trackable.unresolvedFlags = function () {
      return Trackable.findAll({
        where: {
          meta: {
            resolved: false
          }
        },
        include: [
          {
            model: models.TrackableType,
            where: {
              name: 'flag'
            }
          },
          {
            model: models.Enrollment,
            include: ['User']

          }
        ]
      })
    }
  }

  return Trackable
}