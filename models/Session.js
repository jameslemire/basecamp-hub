module.exports = (sequelize, dataTypes) => {
  const Session = sequelize.define('Session', {
    id: {
      type: dataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },

    subject: {
      type: dataTypes.STRING
    },

    name: {
      type: dataTypes.STRING
    },

    location_id: {
      type: dataTypes.INTEGER
    },

    date_start: {
      type: dataTypes.DATE
    },

    date_end: {
      type: dataTypes.DATE
    },

    is_public: {
      type: dataTypes.BOOLEAN
    },

    short_name: {
      type: dataTypes.STRING
    },

    capacity: {
      type: dataTypes.INTEGER
    }

  }, {
    tableName: 'classsession',
    underscored: true
  })

  Session.associate = function (models) {
    Session.hasMany(models.Enrollment, {foreignKey: 'session_id'})
    Session.belongsTo(models.Location, {foreignKey: 'location_id'})
  }

  return Session
}