const whitelist = ['http://localhost:8080', 'http://localhost:4000'];

module.exports = {
  origin: function(origin, cb) {
    if (origin === undefined || whitelist.indexOf(origin) !== -1) {
      cb(null, true);
    } else {
      cb(new Error('Not allowed by CORS'));
    }
  },
  credentials: true
}