const path = require('path')
require('dotenv').config();

module.exports = {
  port: process.env.PORT || 4000,
  db: {
    database: process.env.DB_NAME || 'devmtn',
    user: process.env.DB_USER || 'devmtn',
    password: process.env.DB_PASS || 'password',
    options: {
      retry: {
        match: [
          /SequelizeConnectionError/,
          /SequelizeConnectionRefusedError/,
          /SequelizeHostNotFoundError/,
          /SequelizeHostNotReachableError/,
          /SequelizeInvalidConnectionError/,
          /SequelizeConnectionTimedOutError/,
          /TimeoutError/,
        ],
        name: 'query',
        backoffBase: 100,
        backoffExponent: 1.1,
        timeout: 15000,
        max: 5,
      },
      pool: {
        maxConnections: 100,
        maxIdleTime: 1000
      },
      dialect: process.env.DIALECT || 'postgres',
      host: process.env.HOST || 'postgresdb_server',
      operatorsAliases: false,
    }
  },
  auth: {
    app: 'basecamp_hub',
    client_token: process.env.CLIENT_TOKEN || 'token',
    callbackURL: process.env.AUTH_CALLBACK_URL || 'http://localhost:4000/auth/devmtn/callback',
    jwtSecret: process.env.JWT_SECRET || 'secret'
  }
}